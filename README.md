![WideImg](https://revo300.academy/wp-content/uploads/2019/12/js.jpg)

# [→ Trayecto Programador 2021]
## Técnicas de programación, Programación Orientada a Objetos, Bases de datos y Relaciones Laborales

- Utilizaremos el lenguaje Javascript, desde las bases pero haciendo hincapié en las nuevas características (ECMAScript6).
- Por cada tema habrá ejercicios puntuales a resolver utilizando la plataforma codepend.io; para los proyectos se usará como propuesta los desafíos (Challenges). Los desafíos son proyectos completos que formarán parte del portfolio del alumno. 
- Desde el primer mes se construirá el portfolio que será online, requisito necesario para poder conseguir trabajo.
- Cada vez que un alumno resuleva un ejercicio podrá publicarlo en LinkedIn con el hasgtag #developer[+número de ejercicio].
- Trabajaremos con las [web APIs](https://developer.mozilla.org/en-US/docs/Web/API)

___

### Información sobre el Curso

Las clases en vivo son en vivo los miércoles a las 18.30, con la siguiente propuesta de trabajo:

- Se repasará los temas de la semana de manera resumida
- Despejar dudas de los ejercicios
- Avance de la próxima semana
- Durante la semana se podrán hacer consultas usando slack. Slack es como whatsapp pero para trabajar.

Link en el campus del [Campus - Centro 27](http://vps-1861905-x.dattaweb.com/moodle/)
___


[Temas]
=================
**JAVASCRIPT**


Nombre | Teoría | Ejercicios | Otro
------------ | ---------- | ------------ | ------------
1- Introducción | [Presentación](https://docs.google.com/presentation/d/1OVH3pt-70FajK5FqXJA_l-0FtPepWxuC4lO_ncEdiLk/edit?usp=sharing) ||  
2- Características y sintáxis | [Presentación](https://docs.google.com/presentation/d/1A2APCZo1Omn-i0V3zI_nAmOR3dP0UfWM-pQn_aId3zE/edit?usp=sharing)||
3- Tipos de datos |[Presentación](https://docs.google.com/presentation/d/1j2ZOvtk7NCQGen34q6XZiytvGyK5IqvFBKgkA6mcaic/edit?usp=sharing)||
4- Variables|[Presentación](https://docs.google.com/presentation/d/1mER8A2sPK2PgRatD5Q8javrmztX4tczijjltB6T5Xr4/edit?usp=sharing)||
5- Estructuras de control|[Presentación](https://docs.google.com/presentation/d/19RrV1Pjs9DGkdSZWkfNV5R7XcHf3IhmZiIi8PweGPhc/edit?usp=sharing)||
6- Manejo de errores|[Presentación](https://docs.google.com/presentation/d/106VEr1yBd4QO0u8Ldelx0NGelVgJYXF1LO0Fh6s_wWg/edit?usp=sharing)||
7- Tipo de operadores|[Presentación](https://docs.google.com/presentation/d/1b0arNhEUWKp8hP57U87r9LER_26pF8OhODZNvrBV-R8/edit?usp=sharing)||
8- Break and continue|[Presentación](https://docs.google.com/presentation/d/16MvSdyZcEOwrgEra5bnv6EDevNTWMm36t2biihixRlU/edit?usp=sharing)||
10- Objetos literales|[Presentación](https://docs.google.com/presentation/d/11nhGzGj__Ldbsq8MZIS2VGZwwtjkhPrrd8dPNWBeNuU/edit?usp=sharing)||
11- Parámetros REST & Operador Spread|[Presentación](https://docs.google.com/presentation/d/1yaLuo7iIiUQb1l3pbiVQFkHf9Xwch4kEE4R95Uf6Hrs/edit?usp=sharing)||
12- Arrow functions|[Presentación](https://docs.google.com/presentation/d/14M3pZjlNRPblMmq2ZXZ8hE3R1BVnVu5srqZAxW9bKTY/edit?usp=sharing)||
___


**INFRAESTRUCTURA**


Nombre | Teoría | Ejercicios | Otro
------------ | ---------- | ------------ | ------------
13- Infraestructura|[Presentación](https://docs.google.com/presentation/d/1626Yso2RKrraHraIa3GaNgAhpcPm7nW2L2IvQvBM_-U/edit?usp=sharing)||
___


**WEB**


Nombre | Teoría | Ejercicios | Otro
------------ | ---------- | ------------ | ------------
Evolución de la web:|[Presentación](https://docs.google.com/presentation/d/1uMb-yViv6zbbzaiSJHcUctvh7TCw4RbYNwin8_ZP5pk/edit?usp=sharing)||
Web Browsers (navegadores): |[Presentación](https://docs.google.com/presentation/d/1SwHadVF_SUmAKvGWFJagi6R-gJIrE_ttU_g24Rm9jvE/edit?usp=sharing)||
___


**HTML - INTERFAZ WEB**

Nombre | Teoría | Ejercicios | Otro
------------ | ---------- | ------------ | ------------
Sintaxis |[Presentación](https://docs.google.com/presentation/d/1eaRelwCCTY6joXWO9dTwdgyKS9hOLobGiFQ5unY0c0A/edit?usp=sharing)|[Video](https://youtu.be/XZnEcXGT9Vo)|
Tablas||[Video](https://youtu.be/A8UKdrvXqt4)|
Formularios||[Video](https://youtu.be/CeWArn7P-As)||
HTML 5, tags|||


## Relaciones Laborales
- Formación Universitaria VS Centro de Formación Profesional: [Opinión 1](https://www.youtube.com/watch?v=Yz27Hm1JHSU&ab_channel=latincoder), [Opinión 2](https://www.youtube.com/watch?v=vNQxSNdR26k), [Opinión 3](https://www.youtube.com/watch?v=Bi3MO59Tx5c&ab_channel=EDteam)
- Curriculum Vitae: [Hacer CV desde el marketing](https://www.youtube.com/watch?v=_qHGydQFD4U&ab_channel=EDteam)
- [La importancia de la marca personal](https://www.youtube.com/watch?v=DhZhcBjTbPc&ab_channel=OscarBarajas)
- [Cómo hacer un presupuesto](https://www.youtube.com/watch?v=jnmWa9Dl3vM&ab_channel=EDteam)
- [7 pasos para ser programador autodidacta](https://www.youtube.com/watch?v=IeBA6J4HBB4&ab_channel=ProgramadorX)
- ["No es ético cambiar de trabajo cada 2 años"](https://www.youtube.com/watch?v=SeRULCAM2Gg&ab_channel=HolaMundoHolaMundo)
- [Cómo conseguir tu primer empleo](https://www.youtube.com/watch?v=CrqQ0keKn2U&ab_channel=HolaMundoHolaMundo)
- [Consejos para conseguir tu Primer Trabajo como Programador](https://www.youtube.com/watch?v=TUVRiaK_9eU&ab_channel=hdeleon.nethdeleon.net)
- [¿Necesito el INGLÉS para programar?](https://www.youtube.com/watch?v=NhBGdg5zNyk&ab_channel=ProgramadorXProgramadorX)
